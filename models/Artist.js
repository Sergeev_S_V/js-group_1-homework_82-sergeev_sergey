const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ArtistSchema = new Schema({ // 19:57 - 19:59 listen again
  name: { // 19:59 - 20:01 listen again
    type: String, required: true,
  },
  image: String,
  information: String,
  published: {
    type: Boolean,
    default: false,
  },
  userId: {
    type: String, required: true
  }
});

const Artist = mongoose.model('Artist', ArtistSchema);

module.exports = Artist;