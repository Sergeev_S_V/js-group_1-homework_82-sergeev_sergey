const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AlbumSchema = new Schema({ // 19:57 - 19:59 listen again
  title: { // 19:59 - 20:01 listen again
    type: String, required: true, unique: true,
  },
  artist: {
    type: Schema.Types.ObjectId,
    ref: 'Artist',
    required: true,
  },
  releaseDate: {
    type: String, required: true
  },
  image: String,
  published: {
    type: Boolean,
    default: false,
  },
  userId: {
    type: String, required: true
  }
});

const Album = mongoose.model('Album', AlbumSchema);

module.exports = Album;