const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TrackSchema = new Schema({
  number: {
    type: Number,
    required: true
  },
  title: {
    type: String, required: true, unique: true
  },
  album: {
    type: Schema.Types.ObjectId,
    ref: 'Album',
    required: true
  },
  duration: {
    type: String, required: true
  },
  youtubeLink: String,
  published: {
    type: Boolean,
    default: false,
  },
  userId: {
    type: String, required: true
  }
});

const Track = mongoose.model('Track', TrackSchema);

module.exports = Track;