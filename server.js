const express = require('express');
const mongoose = require('mongoose');
const config = require('./config');
const cors = require('cors');
const albums = require('./app/albums');
const artists = require('./app/artists');
const tracks = require('./app/tracks');
const users = require('./app/users');
const trackHistory = require('./app/trackHistory');


const app = express();

const port = 8000;

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', () => {
  console.log('Mongoose connected!');

  app.use('/artists', artists());
  app.use('/albums', albums());
  app.use('/tracks', tracks());
  app.use('/users', users());
  app.use('/track_history', trackHistory());

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });
});
