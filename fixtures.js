const mongoose = require('mongoose');
const config = require('./config');

const Artist = require('./models/Artist');
const Album = require('./models/Album');
const Track = require('./models/Track');
const User = require('./models/User');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
  try {
    await db.dropCollection('users');
    await db.dropCollection('artists');
    await db.dropCollection('albums');
    await db.dropCollection('tracks');
  } catch (e) {
    console.log('Collections were not present, skipping drop...');
  }

  const [User1, User2, Admin] = await User.create({
    username: 'user1',
    password: '123',
    role: 'user'
  }, {
    username: 'user2',
    password: '123',
    role: 'user'
  }, {
    username: 'admin',
    password: '123',
    role: 'admin'
  });

  const [LinkinPark, LimpBizkit, ThirtySecondsToMars] = await Artist.create({
    name: 'Linkin Park',
    image: 'LP-image.jpg',
    published: false,
    userId: User1._id,
    information: 'Linkin Park — американская мультиплатиновая рок-группа, основанная в 1996 году под названием Xero и исполняющая музыку преимущественно в стилях альтернативный метал, ню-метал и рэп-метал, альтернативный рок, электроник-рок, поп и поп-рок.'
  }, {
    name: 'Limp Bizkit',
    image: 'LimpBizkit-image.jpg',
    published: false,
    userId: User1._id,
    information: 'Limp Bizkit — американская ню-метал/рэп-рок-группа, образованная в 1994 году в городе Джэксонвилл, штат Флорида. В нынешний состав коллектива входят Фред Дёрст, Уэс Борланд, Сэм Риверс и Джон Отто.'
  }, {
    name: 'Thirty Seconds to Mars',
    image: '30STM-image.jpg',
    published: false,
    userId: User2._id,
    information: 'Thirty Seconds to Mars — американская рок-группа из Лос-Анджелеса, штат Калифорния, исполняющая альтернативный рок. Основана в 1998 году братьями Джаредом и Шенноном Лето.'
  });

  const [Meteora, HibridTheory, GoldCobra, ThirtySecondstoMars] = await Album.create({
    title: 'Meteora',
    artist: LinkinPark._id,
    releaseDate: '25 апреля 2003',
    image: 'LP-album-meteora.jpg',
    published: false,
    userId: User1._id,
  }, {
    title: 'Hibrid Theory',
    artist: LinkinPark._id,
    releaseDate: '24 октября 2000 г.',
    image: 'LP-album-hibrid-theory.jpg',
    published: false,
    userId: User1._id,
  }, {
    title: 'Gold Cobra',
    artist: LimpBizkit._id,
    releaseDate: '28 июня 2011 г.',
    image: 'LimpBizkit-album-gold-cobra.jpg',
    published: false,
    userId: User1._id,
  }, {
    title: '30 Seconds to Mars',
    artist: ThirtySecondsToMars._id,
    releaseDate: '27 августа 2002 г.',
    image: '30STM-album-30stm.jpg',
    published: false,
    userId: User2._id,
  });

  await Track.create({
    number: 1,
    title: 'Faint',
    album: Meteora._id,
    duration: '2:42',
    youtubeLink: 'https://www.youtube.com/watch?v=LYU-8IFcDPw',
    published: false,
    userId: User1._id
  }, {
    number: 2,
    title: 'Figure.09',
    album: Meteora._id,
    duration: '3:18',
    youtubeLink: 'https://www.youtube.com/watch?v=Gc9PYWTDKfE',
    published: false,
    userId: User1._id
  }, {
    number: 3,
    title: 'Hit the Floor',
    album: Meteora._id,
    duration: '2:44',
    youtubeLink: 'https://www.youtube.com/watch?v=yQ_BiMRWahU',
    published: false,
    userId: User1._id
  }, {
    number: 4,
    title: 'Don`t Stay',
    album: Meteora._id,
    duration: '3:08',
    youtubeLink: 'https://www.youtube.com/watch?v=NY3VYW1DXH4',
    published: false,
    userId: User1._id
  }, {
    number: 5,
    title: 'Numb',
    album: Meteora._id,
    duration: '3:08',
    youtubeLink: 'https://www.youtube.com/watch?v=kXYiU_JCYtU',
    published: false,
    userId: User1._id
  }, {
    number: 6,
    title: 'Papercut',
    album: HibridTheory._id,
    duration: '3:05',
    youtubeLink: 'https://www.youtube.com/watch?v=vjVkXlxsO8Q',
    published: false,
    userId: User1._id
  }, {
    number: 7,
    title: 'Runaway',
    album: HibridTheory._id,
    duration: '3:04',
    youtubeLink: 'https://www.youtube.com/watch?v=im0CC122xW8',
    published: false,
    userId: User1._id
  }, {
    number: 8,
    title: 'In the End',
    album: HibridTheory._id,
    duration: '3:36',
    youtubeLink: 'https://www.youtube.com/watch?v=eVTXPUF4Oz4',
    published: false,
    userId: User1._id
  }, {
    number: 9,
    title: 'Points of Authority',
    album: HibridTheory._id,
    duration: '3:20',
    youtubeLink: 'https://www.youtube.com/watch?v=jZSPAp8kCl4',
    published: false,
    userId: User1._id
  }, {
    number: 10,
    title: 'One Step Closer',
    album: HibridTheory._id,
    duration: '2:36',
    youtubeLink: 'https://www.youtube.com/watch?v=4qlCC1GOwFw',
    published: false,
    userId: User1._id
  }, {
    number: 11,
    title: 'Gold Cobra',
    album: GoldCobra._id,
    duration: '3:53',
    youtubeLink: 'https://www.youtube.com/watch?v=_i_qxQztHRI',
    published: false,
    userId: User1._id
  }, {
    number: 12,
    title: 'Bring It Back',
    album: GoldCobra._id,
    duration: '2:17',
    youtubeLink: 'https://www.youtube.com/watch?v=B96CQfC2TYY',
    published: false,
    userId: User1._id
  }, {
    number: 13,
    title: 'Shark Attack',
    album: GoldCobra._id,
    duration: '3:26',
    youtubeLink: 'https://www.youtube.com/watch?v=gz3GMlKJc5U',
    published: false,
    userId: User1._id
  }, {
    number: 14,
    title: 'Get A Life',
    album: GoldCobra._id,
    duration: '4:54',
    youtubeLink: 'https://www.youtube.com/watch?v=AtWyHckCzGU',
    published: false,
    userId: User1._id
  }, {
    number: 15,
    title: 'Douche Bag',
    album: GoldCobra._id,
    duration: '3:42',
    youtubeLink: 'https://www.youtube.com/watch?v=fcOob0n0rbE',
    published: false,
    userId: User1._id
  }, {
    number: 16,
    title: 'Capricorn (A Brand New Name)',
    album: ThirtySecondstoMars._id,
    duration: '3:53',
    youtubeLink: 'https://www.youtube.com/watch?v=JiSasfH-xIE',
    published: false,
    userId: User2._id
  }, {
    number: 17,
    title: 'Edge of the Earth',
    album: ThirtySecondstoMars._id,
    duration: '4:36',
    youtubeLink: 'https://www.youtube.com/watch?v=-UreN8Luye8',
    published: false,
    userId: User2._id
  }, {
    number: 18,
    title: 'Oblivion',
    album: ThirtySecondstoMars._id,
    duration: '3:27',
    youtubeLink: 'https://www.youtube.com/watch?v=svceOen4yWg',
    published: false,
    userId: User2._id
  }, {
    number: 19,
    title: 'End of the Beginning',
    album: ThirtySecondstoMars._id,
    duration: '4:37',
    youtubeLink: 'https://www.youtube.com/watch?v=zYpeYClL6kQ',
    published: false,
    userId: User2._id
  }, {
    number: 20,
    title: '93 Million Miles',
    album: ThirtySecondstoMars._id,
    duration: '5:17',
    youtubeLink: 'https://www.youtube.com/watch?v=W1SgIc0pHqs',
    published: false,
    userId: User2._id
  });

  db.close();
});

