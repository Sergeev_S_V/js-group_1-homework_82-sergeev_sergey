const express = require('express');
const Track = require('../models/Track');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const router = express.Router();

const sortByNumber = tracks => { // не сортирует
  return tracks.sort((a, b) => a.number - b.number);
};

const createRouter = () => {
  router.get('/', async (req, res) => {
    const albumId = req.query.album;
    const trackId = req.query.track;

    if (trackId) {

      try {
        const track = await Track.findOne({_id: trackId}).select('youtubeLink');
        const youtubeLink = track.youtubeLink;

        res.send(youtubeLink);
      } catch (e) {
        res.sendStatus(500);
      }

    }

    if (albumId) {

      try {
        let tracks = await Track.find({album: albumId}).populate('album');
        res.send(tracks);
      } catch (e) {
        res.status(500).send(e);
      }

    } else {
      try {
        let tracks = await Track.find().populate('album');
        let sortedTracks = sortByNumber(tracks);

        res.send(sortedTracks);
      } catch (e) {
        res.sendStatus(500);
      }
    }
  });

  router.post('/', auth, async (req, res) => {
    let foundTrackByNumber = await Track.findOne({number: req.body.number, album: req.body.album});

    if (foundTrackByNumber) {
      return res.status(400).send({error: `"${req.body.number}" is not correct number of track. Please enter correct number of track`});
    }

    const trackData = {
      title: req.body.title,
      album: req.body.album,
      duration: req.body.duration,
      youtubeLink: req.body.youtubeLink,
      number: req.body.number,
      userId: req.user._id
    };

    const track = new Track(trackData);

    try {
      let savedTrack = await track.save();
      res.send(savedTrack)
    } catch (e) {
      res.status(400).send(e);
    }
  });

  router.post(`/:id/publish`, auth, async (req, res) => {
    const trackId = req.params.id;
    const albumId = req.body.albumId;

    try {
      const track = await Track.findOneAndUpdate({_id: trackId}, {$set: {published: true}}, {new: true});
      await track.save();
    } catch (e) {
      res.send({error: `${trackId} not correct`});
    }

    try {
      if (albumId) {
        const tracks = await Track.find({album: albumId}).populate('album');
        res.send(tracks);
      } else {
        const tracks = await Track.find().populate('album');
        res.send(tracks);
      }
    } catch (e) {
      res.send(e);
    }
  });

  router.delete(`/:id`, [auth, permit('admin')], async (req, res) => {
    const trackId = req.params.id;

    try {
      await Track.findOneAndRemove({_id: trackId});
      const tracks = await Track.find().populate('album');
      res.send(tracks);
    } catch (e) {
      res.send({error: `Something wrong happened, perhaps the ${trackId} is not correct`});
    }
  });

  return router;
};

module.exports = createRouter;
