const express = require('express');
const trackHistory = require('../models/trackHistory');
const auth = require('../middleware/auth');

const createRouter = () => {
  const router = express.Router();

  router.get('/', auth, async (req, res) => {

    const userId = req.user._id;

    trackHistory.find(userId ? {userId} : null)
      .populate({path: 'trackId',
        populate: {path: 'album',
          populate: {path: 'artist'}}})
      .then(result => res.send(result));
  });

  router.post('/', auth, (req, res) => {
    const trackData = {
      userId: req.user._id,
      trackId: req.body.trackId,
      datetime: new Date()
    };

    const track = new trackHistory(trackData);

    track.save()
      .then(track => res.send(track))
      .catch(error => res.status(400).send(error))
  });

  return router;
};

module.exports = createRouter;