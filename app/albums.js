const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const Album = require('../models/Album');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const sortByReleaseDate = (albums) => {
  return albums.sort((a, b) => a.releaseDate < b.releaseDate);
};

const upload = multer({storage});

const router = express.Router();

const createRouter = () => { // 19-55 - 19:57 listen again
  router.get('/', async (req, res) => { // 20:44 - 20:45
    const artistId = req.query.artist;
    if (artistId) {
      Album.find({artist: artistId}).populate('artist')
        .then(result => res.send(result))
    } else {
      try {
        let albums = await Album.find().populate('artist');
        let sortedAlbums = sortByReleaseDate(albums);

        res.send(sortedAlbums);
      } catch (e) {
        res.sendStatus(500)
      }
    }
  });

  router.post('/', [auth, upload.single('image')], (req, res) => {
    if (!req.body.title || !req.body.artist || !req.body.releaseDate) {
      return res.status(400).send({error: 'Please, fill all required fields'});
    }

    const albumData = {
      title: req.body.title,
      artist: req.body.artist,
      releaseDate: req.body.releaseDate,
      userId: req.user._id
    };

    if (req.file) {
      albumData.image = req.file.filename;
    } else {
      albumData.image = null;
    }

    const album = new Album(albumData);

    album.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });

  router.get('/:id', (req, res) => {
    const albumId = req.params.id;
    Album.findOne({_id: albumId}, (err, result) => {
      if (err) res.status(404).send(err);
      else res.send(result);
    }).populate('artist');

  });

  router.post(`/:id/publish`, auth, async (req, res) => {
    const albumId = req.params.id;
    const artistId = req.body.artistId;

    try {
      const album = await Album.findOneAndUpdate({_id: albumId}, {$set: {published: true}}, {new: true});
      await album.save();
    } catch (e) {
      res.send({error: `${albumId} not correct`});
    }

    try {
      if (artistId) {
        const albums = await Album.find({artist: artistId}).populate('artist');
        res.send(albums);
      } else {
        const albums = await Album.find().populate('artist');
        res.send(albums);
      }
    } catch (e) {
      res.send(e);
    }
  });

  router.delete(`/:id`, [auth, permit('admin')], async (req, res) => {
    const albumId = req.params.id;

    try {
      await Album.findOneAndRemove({_id: albumId});
      const albums = await Album.find().populate('artist');
      res.send(albums);
    } catch (e) {
      res.send({error: `Something wrong happened, perhaps the ${albumId} is not correct`});
    }
  });

  return router; // 20:28 - 20:32 про роутер и json
};

module.exports = createRouter;