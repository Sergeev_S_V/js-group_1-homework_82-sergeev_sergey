const express = require('express');
const multer = require("multer");
const nanoid = require("nanoid");
const path = require('path');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const Artist = require('../models/Artist');
const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

const createRouter = () => {
  router.get('/', (req, res) => {
    Artist.find()
      .then(results => res.send(results))
      .catch(() => res.sendStatus(500));
  });

  router.post('/', [auth, upload.single('image')], (req, res) => {
    if (!req.body.name) {
      res.status(400).send({error: 'Please, fill all required fields'});
    }

    const artistData = {
      name: req.body.name,
      information: req.body.information,
      userId: req.user._id
    };

    if (req.file) {
      artistData.image = req.file.filename;
    } else {
      artistData.image = null;
    }

    const artist = new Artist(artistData);

    artist.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });

  router.post(`/:id/publish`, auth, async (req, res) => {
    const artistId = req.params.id;

    try {
      const artist = await Artist.findOneAndUpdate({_id: artistId}, {$set: {published: true}}, {new: true});
      await artist.save();
    } catch (e) {
      res.send({error: `${artistId} not correct`});
    }

    try {
      const artists = await Artist.find();
      res.send(artists);
    } catch (e) {
      res.send(e);
    }
  });

  router.delete(`/:id`, [auth, permit('admin')], async (req, res) => {
    const artistId = req.params.id;

    try {
      await Artist.findOneAndRemove({_id: artistId});
      const artists = await Artist.find();
      res.send(artists);
    } catch (e) {
      res.send({error: `Something wrong happened, perhaps the ${artistId} is not correct`});
    }
  });

  return router;
};

module.exports = createRouter;
